import java.io.*;
import java.util.*;

// **********************************************************************
// Ast class (base class for all other kinds of nodes)
// **********************************************************************
abstract class Ast {
}

class Program extends Ast {

    public Program(DeclList declList) {
        this.declList = declList;
    }
    
    // Semantic checking
    public void check()
    {
        declList.check();
    }

    protected DeclList declList;
}

// **********************************************************************
// Decls
// **********************************************************************
class DeclList extends Ast {

    public DeclList(LinkedList decls) {
        this.decls = decls;
    }

    boolean nameCheck(LinkedList<String> names, String name)  {
    	for (int i = 0; i<names.size(); i++) {
    		if (names.get(i).equals(name))
    			return true; 
    	}
    	return false;
    }
    // linked list of kids (Decls)
    protected LinkedList decls;
    void check() {
    	LinkedList<String> names = new LinkedList<String>();
    	for (int i=0; i<decls.size(); i++)
    	{	
   		Decl dec =  (Decl) decls.get(i);
   		if (names.contains(dec.name.strVal))
    			Errors.semanticError(dec.name.lineNum, dec.name.charNum, dec.name.strVal + "Already defined you!");
    		names.add(dec.name.strVal);
    		
    		System.out.println(dec.name.strVal);
    	}
    }
}

abstract class Decl extends Ast {
	protected Type type;
    protected Id name;
    
   
}

class VarDecl extends Decl {

    public VarDecl(Type type, Id name) {
        this.type = type;
        this.name = name;
    }

   
    
}

class FnDecl extends Decl {

    public FnDecl(Type type, Id name, FormalsList formalList, FnBody body) {
        this.type = type;
        this.name = name;
        this.formalList = formalList;
        this.body = body;
    }

    
    protected FormalsList formalList;
    protected FnBody body;
}

class FnPreDecl extends Decl {

    public FnPreDecl(Type type, Id name, FormalsList formalList) {
        this.type = type;
        this.name = name;
        this.formalList = formalList;
    }

    
    protected FormalsList formalList;
}

class FormalsList extends Ast {

    public FormalsList(LinkedList formals) {
        this.formals = formals;
    }

    // linked list of kids (FormalDecls)
    protected LinkedList formals;
}

class FormalDecl extends Decl {

    public FormalDecl(Type type, Id name) {
        this.type = type;
        this.name = name;
    }

    protected Type type;
    protected Id name;
}

class FnBody extends Ast {

    public FnBody(DeclList declList, StmtList stmtList) {
        this.declList = declList;
        this.stmtList = stmtList;
    }

    protected DeclList declList;
    protected StmtList stmtList;
}

class StmtList extends Ast {

    public StmtList(LinkedList stmts) {
        this.stmts = stmts;
    }

    // linked list of kids (Stmts)
    protected LinkedList stmts;
}

// **********************************************************************
// Types
// **********************************************************************
class Type extends Ast {
    
    protected Type() {}
    
    public static Type CreateSimpleType(String name)
    {
        Type t = new Type();
        t.name = name;
        t.size = -1;
        t.numPointers = 0;
        
        return t;
    }
    public static Type CreateArrayType(String name, int size)
    {
        Type t = new Type();
        t.name = name;
        t.size = size;
        t.numPointers = 0;
        
        return t;
    }
    public static Type CreatePointerType(String name, int numPointers)
    {
        Type t = new Type();
        t.name = name;
        t.size = -1;
        t.numPointers = numPointers;
        
        return t;
    }
    public static Type CreateArrayPointerType(String name, int size, int numPointers)
    {
        Type t = new Type();
        t.name = name;
        t.size = size;
        t.numPointers = numPointers;
        
        return t;
    }
    
    public String name()
    {
        return name;
    }
 
    protected String name;
    protected int size;  // use if this is an array type
    protected int numPointers;
    
    public static final String voidTypeName = "void";
    public static final String intTypeName = "int";
}

// **********************************************************************
// Stmts
// **********************************************************************
abstract class Stmt extends Ast {
}

class AssignStmt extends Stmt {

    public AssignStmt(Exp lhs, Exp exp) {
        this.lhs = lhs;
        this.exp = exp;
    }

    protected Exp lhs;
    protected Exp exp;
}

class IfStmt extends Stmt {

    public IfStmt(Exp exp, DeclList declList, StmtList stmtList) {
        this.exp = exp;
        this.declList = declList;
        this.stmtList = stmtList;
    }
    
    protected Exp exp;
    protected DeclList declList;
    protected StmtList stmtList;
}

class IfElseStmt extends Stmt {

    public IfElseStmt(Exp exp, DeclList declList1, StmtList stmtList1, 
            DeclList declList2, StmtList stmtList2) {
        this.exp = exp;
        this.declList1 = declList1;
        this.stmtList1 = stmtList1;
        this.declList2 = declList2;
        this.stmtList2 = stmtList2;
    }

    protected Exp exp;
    protected DeclList declList1;
    protected DeclList declList2;
    protected StmtList stmtList1;
    protected StmtList stmtList2;
}

class WhileStmt extends Stmt {

    public WhileStmt(Exp exp, DeclList declList, StmtList stmtList) {
        this.exp = exp;
        this.declList1 = declList1;
        this.stmtList = stmtList;
    }

    protected Exp exp;
    protected DeclList declList1;
    protected StmtList stmtList;
}

class ForStmt extends Stmt {

    public ForStmt(Stmt init, Exp cond, Stmt incr, 
            DeclList declList1, StmtList stmtList) {
        this.init = init;
        this.cond = cond;
        this.incr = incr;
        this.declList1 = declList1;
        this.stmtList = stmtList;
    }

    protected Stmt init;
    protected Exp cond;
    protected Stmt incr;
    protected DeclList declList1;
    protected StmtList stmtList;
}

class CallStmt extends Stmt {

    public CallStmt(CallExp callExp) {
        this.callExp = callExp;
    }

    protected CallExp callExp;
}

class ReturnStmt extends Stmt {

    public ReturnStmt(Exp exp) {
        this.exp = exp;
    }

    protected Exp exp; // null for empty return
}

// **********************************************************************
// Exps
// **********************************************************************
abstract class Exp extends Ast {
    public abstract int getLine();
    public abstract int getChar();
}

abstract class BasicExp extends Exp
{
    protected int lineNum;
    protected int charNum;
    
    public BasicExp(int lineNum, int charNum)
    {
        this.lineNum = lineNum;
        this.charNum = charNum;
    }
    
    public int getLine()
    {
        return lineNum;
    }
    public int getChar()
    {
        return charNum;
    }
}

class IntLit extends BasicExp {

    public IntLit(int lineNum, int charNum, int intVal) {
        super(lineNum, charNum);
        this.intVal = intVal;
    }
 
    protected int intVal;
}

class StringLit extends BasicExp {

    public StringLit(int lineNum, int charNum, String strVal) {
        super(lineNum, charNum);
        this.strVal = strVal;
    }

    public String str() {
        return strVal;
    }
    
    protected String strVal;
}

class Id extends BasicExp {

    public Id(int lineNum, int charNum, String strVal) {
        super(lineNum, charNum);
        this.strVal = strVal;
    }

    protected String strVal;
   
}

class ArrayExp extends Exp {

    public ArrayExp(Exp lhs, Exp exp) {
        this.lhs = lhs;
        this.exp = exp;
    }

    public int getLine() {
        return lhs.getLine();
    }

    public int getChar() {
        return lhs.getChar();
    }

    protected Exp lhs;
    protected Exp exp;
}

class CallExp extends Exp {

    public CallExp(Id name, ActualList actualList) {
        this.name = name;
        this.actualList = actualList;
    }

    public CallExp(Id name) {
        this.name = name;
        this.actualList = new ActualList(new LinkedList());
    }

    public int getLine() {
        return name.getLine();
    }

    public int getChar() {
        return name.getChar();
    }

    protected Id name;
    protected ActualList actualList;
}
class ActualList extends Ast {

    public ActualList(LinkedList exps) {
        this.exps = exps;
    }

    // linked list of kids (Exps)
    protected LinkedList exps;
}

abstract class UnaryExp extends Exp {

    public UnaryExp(Exp exp) {
        this.exp = exp;
    }

    public int getLine() {
        return exp.getLine();
    }

    public int getChar() {
        return exp.getChar();
    }

    protected Exp exp;
}

abstract class BinaryExp extends Exp {

    public BinaryExp(Exp exp1, Exp exp2) {
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    public int getLine() {
        return exp1.getLine();
    }

    public int getChar() {
        return exp1.getChar();
    }

    protected Exp exp1;
    protected Exp exp2;
}


// **********************************************************************
// UnaryExps
// **********************************************************************
class UnaryMinusExp extends UnaryExp {

    public UnaryMinusExp(Exp exp) {
        super(exp);
    }
}

class NotExp extends UnaryExp {

    public NotExp(Exp exp) {
        super(exp);
    }
}

class AddrOfExp extends UnaryExp {

    public AddrOfExp(Exp exp) {
        super(exp);
    }
}

class DeRefExp extends UnaryExp {

    public DeRefExp(Exp exp) {
        super(exp);
    }
}

// **********************************************************************
// BinaryExps
// **********************************************************************
class PlusExp extends BinaryExp {

    public PlusExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class MinusExp extends BinaryExp {

    public MinusExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class TimesExp extends BinaryExp {

    public TimesExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class DivideExp extends BinaryExp {

    public DivideExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class ModuloExp extends BinaryExp {

    public ModuloExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class AndExp extends BinaryExp {

    public AndExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class OrExp extends BinaryExp {

    public OrExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class EqualsExp extends BinaryExp {

    public EqualsExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class NotEqualsExp extends BinaryExp {

    public NotEqualsExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class LessExp extends BinaryExp {

    public LessExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class GreaterExp extends BinaryExp {

    public GreaterExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class LessEqExp extends BinaryExp {

    public LessEqExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}

class GreaterEqExp extends BinaryExp {

    public GreaterEqExp(Exp exp1, Exp exp2) {
        super(exp1, exp2);
    }
}